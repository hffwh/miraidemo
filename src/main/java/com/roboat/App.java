package com.roboat;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.event.events.GroupMessageEvent;
import net.mamoe.mirai.message.data.*;
import net.mamoe.mirai.utils.ExternalResource;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Hello world!
 *
 */
public class App {

    //配置文件
    private static Properties properties;
    static {
        try {
           properties = new Properties();
           properties.load(new FileInputStream(new File("./src/main/resources/config.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main( String[] args ) {

        //输入qq账号密码登陆QQ(如果是第一次登陆需要手机辅助验证,登陆成功后会在缓存文件夹创建登陆所需指纹文件)
        Scanner scanner = new Scanner(System.in);
        Utils utils = new Utils();
        System.out.println("请输入qq号码");
        Long qq = scanner.nextLong();
        System.out.println("请输入qq密码");
        String password = scanner.next();
        System.out.println(qq+":"+password);
        Bot bot = utils.login(qq,password);

        //创建监听器,指定监听类型为群组事件类型
        bot.getEventChannel().subscribeAlways(GroupMessageEvent.class,event -> {
            //事件触发的回调,even为事件对象
            eventHandler(event);
            //全局消息处理
            String message = event.getMessage().contentToString();
            String ImageId = "";
            for (SingleMessage singleMessage : event.getMessage()) {
                if (singleMessage instanceof Image){
                    ImageId = ((Image) singleMessage).getImageId();
                }
            }
            if (message.contains("垃圾")){
                InputStream inputStream = null;
                try {
                    //输出一个回收站的截图嘿嘿
                    inputStream = new FileInputStream(new File("C:\\Users\\马保国\\Desktop\\垃圾.png"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                ExternalResource.sendAsImage(inputStream, event.getGroup());
                return;
            }
            if (ImageId.equals("{1DB34403-D6DA-5C2C-C3D2-EF86C4D5E7CF}.gif")){
                event.getGroup().sendMessage(Image.fromId("{1DB34403-D6DA-5C2C-C3D2-EF86C4D5E7CF}.gif"));
            }
            if (message.contains("@") && message.contains("爬")){
                String regEx="[^0-9]";
                Pattern pattern = Pattern.compile(regEx);
                Matcher matcher = pattern.matcher(message);
                Long qqNum = Long.valueOf(matcher.replaceAll("").trim());
                InputStream inputStream = Utils.qqPa(qqNum);
                if (inputStream!=null){
                    ExternalResource.sendAsImage(inputStream,event.getGroup());
                }
            }
        });
    }

    /**
     * 消息事件处理
     * @param event 事件对象
     */
    public static void eventHandler(GroupMessageEvent event){
        //获取事件消息
        MessageChain message = event.getMessage();
        //获取发送者id
        long senderId = event.getSender().getId();
        //消息中的ImageId
        List<String> imageIdList = new ArrayList<>();
        for (SingleMessage singleMessage : message) {
            if (singleMessage instanceof Image){
                imageIdList.add(((Image) singleMessage).getImageId());
            }
        }
        //是否@bot
        if (message.contentToString().contains("@"+ event.getBot().getId())){
            //消息处理
            Map<String, Object> resultMap = parseMessage(message.contentToString(), senderId);
            //结果类型
            String type = resultMap.get("type").toString();
            //结果数据
            Object result = resultMap.get("result");

            //返回结果为空
            if (result==null||(result.toString().isEmpty())){
                event.getGroup().sendMessage(Image.fromId("{D1D0B27E-0746-4BB3-6471-88FE7C9F67D5}.jpg"));
                return;
            }
            if (("text").equals(type)){
                //文本消息
                event.getGroup().sendMessage(result.toString());
            }else if (("image").equals(type)){
                //图片消息
                ExternalResource.sendAsImage((InputStream)result,event.getGroup());
            }else{
                //音乐分享
                event.getGroup().sendMessage((MusicShare)result);
            }
        }
    }



    /**
     * 处理消息
     */
    public static Map<String, Object> parseMessage(String message,Long senderID){
        Map<String, Object> map = new HashMap<>();
        try{
            if (message.contains("添加功能")){
                //添加功能以|分隔获取key:和value:后的值,value后的值应为JSON({"url":"","method":"get","xpath":"","type":"text","param":{"key1":"value1","key2":"value2"}})
                //{"url":"","method":"get","xpath":"","type":"text","param":{"key1":"value1","key2":"value2"},"jsonKey":""}

                //权限管理
                String[] split = StrUtil.split(message, "|");
                String[] split1 = StrUtil.split(split[1], "key:");
                String[] split2 = StrUtil.split(split[2], "value:");
                String key = split1[1];
                String value = split2[1];
                System.out.println("功能key:"+key);
                System.out.println(key+"对应的value:"+value);
                properties.setProperty(key,value);
                properties.store(new FileOutputStream("./src/main/resources/config.properties"),key);
                map.put("type","text");
                map.put("result","添加成功");
                return map;
            }
            else if (message.contains("配置列表")){
                Set<Object> keySet = properties.keySet();
                String str = "";
                for (Object o : keySet) {
                    str += o.toString()+"\n";
                }
                map.put("type","text");
                map.put("result",str);
                return map;
            }
            else if (message.contains("点歌")){
                //获取消息内容,判断是否包含歌名
                String str2 = null;
                try {
                    String str1 = message.substring(0, message.indexOf(":"));
                    str2 = message.substring(str1.length()+1, message.length());
                } catch (Exception e) {
                    map.put("type","text");
                    map.put("result","可以这样试试-点歌:好运来");
                    return map;
                }
                //获取歌曲信息
                try {
                    Map<String, String> musicMap = Utils.musicMessage(str2);
                    //创建歌曲分享对象
                    MusicShare musicShare = new MusicShare(MusicKind.NeteaseCloudMusic,musicMap.get("songName")
                            ,musicMap.get("songName"),musicMap.get("songUrl"),musicMap.get("songImg"),musicMap.get("songUrl"));
                    //发送歌曲分享
                    map.put("type","music");
                    map.put("result",musicShare);
                    return map;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                map.put("type","text");
                map.put("result","你在说什么勾八歌曲");
                return map;
            }
            else if (message.contains("删除功能")){
                //权限管理
                if (senderID!=2506694095L){
                    map.put("type","text");
                    map.put("result","男酮快爬");
                    return map;
                }
                String key = message.split(":")[1];
                properties.remove(key);
                properties.store(new FileOutputStream("./src/main/resources/config.properties"),key);
                map.put("type","text");
                map.put("result","删除成功");
                return map;
            }
            else if (message.contains("+")){
                message = message.split("/")[1];
                String[] split = StrUtil.split(message.replace("@","").strip(), "+");
                String emoji1 = split[0];
                String emoji2 = split[1];
                System.out.println(emoji1+"--"+emoji2);
                InputStream inputStream = Utils.emojiMix(emoji1,emoji2);
                map.put("type","image");

                if (inputStream==null){
                    inputStream = new FileInputStream("./src/lib/emojiMixError.png");
                }
                map.put("result",inputStream);
                return map;
            }
            else if (message.contains("ste")){
                String[] s = message.split("ste ");
                String str = s[s.length-1];
                map.put("type","text");
                map.put("result",Utils.strToEmoji(str));
                return map;
            }
            else if (message.contains("show")){
                String s = message.split("show ")[1];
                String value = properties.getProperty(s);
                map.put("type","text");
                map.put("result",value);
                return map;
            }
            else {
                return getProperties(message);
            }
        }catch (Exception e){
            map.put("type","text");
            map.put("result","error:"+e.getMessage());
            return map;
        }
    }

    /**
     * 从properties中读取keyset判断是否包含消息内容
     * @param message 消息内容
     * @return 如果包含消息内容返回响应消息,否则返回null
     */
    public static Map<String, Object> getProperties(String message) {
        HashMap<String, Object> resultMap = new HashMap<>(1);
        String key = message.split(" ")[1];
        Set<Object> keySet = properties.keySet();
        System.out.println(key);
        if (keySet.contains(key)) {
            String value = properties.getProperty(key);
            JSONObject jsonObject = JSONUtil.parseObj(value);
            //消息类型为文本
            if (jsonObject.get("type").toString().equals("text")) {
                //判断响应体是否为JSON
                Object jsonKey = jsonObject.get("jsonKey");
                String textSpider = null;
                if (jsonKey!=null && (!jsonKey.toString().isEmpty())){
                    textSpider = Utils.textSpider(jsonObject.get("url").toString(), jsonObject.get("xpath").toString(), jsonObject.get("method").toString(),jsonKey.toString());
                }else {
                    textSpider = Utils.textSpider(jsonObject.get("url").toString(), jsonObject.get("xpath").toString(), jsonObject.get("method").toString(),"");
                }
                resultMap.put("type", "text");
                resultMap.put("result", textSpider);
                return resultMap;

            }
            //消息类型为图片
            else {
                //判断响应体是否为JSON
                Object jsonKey = jsonObject.get("jsonKey");
                String url = jsonObject.get("url").toString();
                String xpath = jsonObject.get("xpath").toString();
                String method = jsonObject.get("method").toString();
                InputStream inputStream = null;
                if (jsonKey!=null && (!jsonKey.toString().isEmpty())){
                    inputStream = Utils.imageSpider(url,xpath,method, jsonObject.get("param", HashMap.class),jsonKey.toString());
                }else {
                    inputStream = Utils.imageSpider(url,xpath,method, jsonObject.get("param", HashMap.class),"");
                }
                resultMap.put("type", "image");
                resultMap.put("result", inputStream);
                return resultMap;
            }
        } else {
            resultMap.put("type", "text");
            resultMap.put("result", "");
            return resultMap;
        }
    }
}
