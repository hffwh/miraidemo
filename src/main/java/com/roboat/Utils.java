package com.roboat;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.net.URLEncoder;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.BotFactory;
import net.mamoe.mirai.utils.BotConfiguration;
import org.jsoup.Jsoup;


import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;

/**
 * @author 马保国
 */
public class Utils {
    //配置文件
    private static Properties properties;
    static {
        try {
            properties = new Properties();
            properties.load(new FileInputStream(new File("./src/main/resources/config.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 构建机器人对象
     * @return 返回机器人实例
     */
    public Bot login(Long qqNum,String password) {
        //3395415516  + My210425.
        //
        //qq号
        long qq = qqNum;

        //设置机器人配置
        BotConfiguration botConfiguration = new BotConfiguration();
        //心跳检测机制
        botConfiguration.setHeartbeatStrategy(BotConfiguration.HeartbeatStrategy.STAT_HB);
        //通讯协议为安卓手机
        botConfiguration.setProtocol(BotConfiguration.MiraiProtocol.ANDROID_WATCH);
        //设置运行目录(当前为用户根目录)
        botConfiguration.setWorkingDir(new File(System.getProperties().getProperty("user.home")));
        //设置缓存目录
        botConfiguration.setCacheDir(new File("cache"));

        //创建机器人实例并登陆后返回实例对象
        Bot bot = BotFactory.INSTANCE.newBot(qq,password,botConfiguration);

        //登陆机器人
        bot.login();
        return bot;
    }

    /**
     * 舔狗日记
     * @return 返回舔狗语录
     */
    private  String dogMessage() {
        //向舔狗日记API发送请求
        HttpRequest httpRequest = HttpRequest.get("https://api.oick.cn/dog/api.php");
        HttpResponse response = httpRequest.execute();
        //返回响应体
        return response.body();
    }

    /**
     * 获取*图
     * @return 返回*图片地址
     */
    public  String setuPic(){
        //*图接口地址
        String url = "http://api.lolicon.app/setu/v2";
        HttpRequest httpRequest = HttpRequest.get(url);
        HttpResponse response = httpRequest.execute();
        //解析响应体内容
        String jsonString = response.body();
        cn.hutool.json.JSONObject jsonObject = JSONUtil.parseObj(jsonString);
        cn.hutool.json.JSONArray data = jsonObject.getJSONArray("data");
        cn.hutool.json.JSONObject jsonObject1 = JSONUtil.parseObj(data.get(0));
        Object urls = jsonObject1.get("urls");
        cn.hutool.json.JSONObject jsonObject2 = JSONUtil.parseObj(urls);
        Object original = jsonObject2.get("original");
        //返回*图地址
        return original.toString();
    }

    /**
     * tnnd
     * @return 返回tnnd图片地址
     */
    public  String fulijiPic(){
        //向接口发送请求
        HttpRequest httpRequest = HttpRequest.get("https://api.vvhan.com/api/mobil.girl?type=https");
        HttpResponse response = httpRequest.execute();
        String location = response.header("location");
        System.out.println(location);
        //返回响应体
        return location;
    }

    /**
     * 土味情话
     * @return 返回土味情话内容
     */
    public  String loveMessage(){
        //向接口发送请求
        HttpRequest httpRequest = HttpRequest.get("https://api.vvhan.com/api/sao");
        HttpResponse response = httpRequest.execute();
        //返回响应内容
        return response.body();
    }


    /**
     * 爬
     * @param QQnum 需要生成图片对应的QQ号
     * @return 返回图片的输入流
     */
    public  InputStream paPic(Long QQnum){
        //访问接口生成图片
        HttpRequest httpRequest = HttpRequest.get("http://api.klizi.cn/API/ce/paa.php?qq=" + QQnum.toString());
        HttpResponse response = httpRequest.execute();
        return response.bodyStream();
    }

    /**
     * 点歌
     * @param songName 歌曲名称
     * @return 返回歌曲参数
     */
    public static  Map<String, String> musicMessage(String songName){
        Map<String, String> map = new HashMap<>();
        //向网易云发送歌曲的查询链接
        String encode = URLEncoder.createDefault().encode(songName, Charset.forName("UTF-8"));
        HttpRequest httpRequest = HttpRequest.get("http://music.163.com/api/search/get?type=1&s="+encode);
        HttpResponse httpResponse = httpRequest.execute();
        JSONObject resp = JSON.parseObject(httpResponse.body());
        JSONObject result = JSON.parseObject(resp.getString("result"));
        JSONArray songs = result.getJSONArray("songs");
        //获取第一首歌曲解析需要的内容
        Object o = songs.get(0);
        JSONObject song = JSON.parseObject(o.toString());
        Object id = song.get("id");
        Object name = song.get("name");
        Object artists = song.get("artists");
        JSONArray jsonArray = JSON.parseArray(artists.toString());
        JSONObject artistsJson = JSON.parseObject(jsonArray.getString(0));
        Object img1v1Url = artistsJson.get("img1v1Url");
        httpRequest.setUrl("https://api.oick.cn/wyy/api.php?id="+id);
        HttpResponse response = httpRequest.execute();
        String songUrl = response.header("Location");
        map.put("songUrl",songUrl);
        map.put("songName",name.toString());
        map.put("songImg",img1v1Url.toString());
        return map;
    }


    /**
     * 获取文字内容
     * @param url 链接地址
     * @param xpath xpath路径,如果为空直接返回响应体内容
     * @param method 请求方式(get||post)
     * @return 返回获取到的字符串
     */
    public static String textSpider(String url,String xpath,String method,String jsonKey){
        HttpResponse httpResponse = null;
        if (method==null||method.isEmpty()){
            httpResponse = HttpRequest.get(url).execute();
        }else {
            httpResponse = HttpRequest.post(url).execute();
        }
        if (xpath==null||xpath.isEmpty()){
            return httpResponse.body();
        }
        //todo 请求参数的添加,以及无需请求的文字功能的添加,xpath获取多个元素内容
        return Jsoup.parse(httpResponse.body()).selectXpath(xpath).text();
    }

    /**
     * 获取图片内容
     * @param url 链接地址
     * @param xpath 要爬取的Xpath
     * @return 返回爬取的图片的输入流
     */
    public static InputStream imageSpider(String url,String xpath,String method,Map<String, String> param,String jsonKey) {
        HttpRequest httpRequest = null;
        if (method.equals("get")){
            httpRequest = HttpRequest.get(url);
        }else {
            httpRequest = HttpRequest.post(url);
        }
        if (param == null || param.size() == 0) {
            HttpResponse httpResponse = httpRequest.execute();
            if (jsonKey.isEmpty()){
                if (xpath == null||xpath.isEmpty()) {
                    return httpResponse.bodyStream();
                }
                String src = Jsoup.parse(httpResponse.body()).selectXpath(xpath).attr("src");
                return HttpRequest.get(src).execute().bodyStream();
            }else {
                String jsonValue = parseJson(httpResponse.body().toString(), jsonKey);
                return HttpRequest.get(jsonValue).execute().bodyStream();
            }
        } else {
            Set<String> keySet = param.keySet();
            url = url+"?";
            for (String key : keySet) {
                String value = param.get(key);
                url = url+key+"="+value+"&";
            }
            HttpResponse httpResponse = httpRequest.setUrl(url).execute();
            if (jsonKey.isEmpty()){
                if (xpath == null||xpath.isEmpty()) {
                    return httpResponse.bodyStream();
                }
                String src = Jsoup.parse(httpResponse.body()).selectXpath(xpath).attr("src");
                return HttpRequest.get(src).execute().bodyStream();
            }else {
                String jsonValue = parseJson(httpResponse.body(), jsonKey);
                return HttpRequest.get(jsonValue).execute().bodyStream();
            }
        }
    }


    /**
     * 递归寻找JSON里指定key的value,返回第一个找到的value
     * @param json JSON串
     * @param key 要查找的key
     * @return 返回查找到的value值,没有找到返回null
     */
    public static String parseJson(String json,String key){
        cn.hutool.json.JSONObject jsonObject = JSONUtil.parseObj(json);
        if (jsonObject.get(key)==null){
            Set<String> keySet = jsonObject.keySet();
            for (String k : keySet) {
                if (Map.class.isInstance(jsonObject.get(k))){
                    return parseJson(jsonObject.get(k).toString(),key);
                }else if (List.class.isInstance(jsonObject.get(k))){
                    cn.hutool.json.JSONArray jsonArray = JSONUtil.parseArray(jsonObject.get(k));
                    for (Object o : jsonArray) {
                        return parseJson(o.toString(),key);
                    }
                }
            }
        }else {
            return jsonObject.get(key).toString();
        }
        return null;
    }



    public static InputStream emojiMix(String emoji1,String emoji2) throws Exception {
        //读取文件内容
        FileInputStream fileInputStream = new FileInputStream(new File("./src/lib/emoji.txt"));
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        String s = null;
        List<String> emojiList = new ArrayList<>();
        while ((s = bufferedReader.readLine())!= null){
            emojiList.add(s.trim());
        }
        bufferedReader.close();
        fileInputStream.close();
        //要转换的emoji
        Map<String,String> map = new HashMap<>();
        //查找emoji的date和encode
        for (String emoji : emojiList) {
            String[] split = emoji.split(",");
            String fileEmoji = split[0].trim();
            String encode = split[1].trim();
            String date = split[3];
            if (fileEmoji.equals(emoji1)){
                map.put(emoji1,date);
                map.put("emoji1",encode);
            }
            if (fileEmoji.equals(emoji2)){
                map.put(emoji2,date);
                map.put("emoji2",encode);
            }
        }
        if (map.get(emoji1)==null||map.get(emoji2)==null){
            return null;
        }

        Integer date1 = Integer.parseInt(map.get(emoji1));
        Integer date2 = Integer.parseInt(map.get(emoji2));
        String date = date1.toString();
        String encode1 = map.get("emoji1");
        String encode2 = map.get("emoji2");
        //日期大的emoji排在第二位
        String url = "https://www.gstatic.com/android/keyboard/emojikitchen/"+date+"/"+encode1+"/"+encode1+"_"+encode2+".png";
        if (date2>date1){
            date = date2.toString();
            url = "https://www.gstatic.com/android/keyboard/emojikitchen/"+date+"/"+encode2+"/"+encode2+"_"+encode1+".png";
        }
        HttpResponse httpResponse = HttpRequest.get(url).execute();
        System.out.println(httpResponse.getStatus());
        if (httpResponse.getStatus()==200){
            return httpResponse.bodyStream();
        }else {
            url = "https://www.gstatic.com/android/keyboard/emojikitchen/"+date+"/"+encode2+"/"+encode2+"_"+encode1+".png";
            System.out.println(url);
            HttpResponse response = HttpRequest.get(url).execute();
            System.out.println(response.getStatus());
            if (response.getStatus()==200){
                return response.bodyStream();
            }
            return null;
        }
    }


    public static String strToEmoji(String text){
        String encodeText = URLEncoder.createDefault().encode(text, StandardCharsets.UTF_8);
        HttpResponse response = HttpRequest.post("https://www.emojidaquan.com/Transfer/index")
                .header("Accept", "application/json, text/javascript, */*; q=0.01")
                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .body("text="+encodeText+"&"+"mode=3")
                .execute();
        String body = response.body();
        String s = Convert.unicodeToStr(body);
        String msg = JSONUtil.parseObj(s).get("msg").toString();
        return msg;
    }

    public static InputStream qqPa(Long qqNum){

        try{
            //从文件夹中取随机图片进行合成
            File file = new File("./src/lib/img");
            File[] files = file.listFiles();
            Random random = new Random();
            int r = random.nextInt(files.length);
            File pa = files[r];
            //https://q1.qlogo.cn/g?b=qq&nk=975959057&s=640 头像获取接口
            InputStream face = HttpRequest.get("https://q1.qlogo.cn/g?b=qq&nk="+ qqNum + "&s=640" ).execute().bodyStream();
            BufferedImage img = new BufferedImage(500,500,BufferedImage.TYPE_INT_RGB);
            //图片
            BufferedImage paImage = ImageIO.read(pa);
            //头像
            BufferedImage qqFace = ImageIO.read(face);
            //头像切为圆形
            //压缩图片
            qqFace = scaleByPercentage(qqFace,qqFace.getWidth(),qqFace.getWidth());

            int width = qqFace.getWidth();
            int height = qqFace.getHeight();
            BufferedImage qqFaceImage = new BufferedImage(width, width, BufferedImage.TYPE_4BYTE_ABGR);
            Graphics2D graphics = qqFaceImage.createGraphics();
            //切成圆形
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
            //留一个像素的空白
            int border = 1;
            Ellipse2D.Double shape = new Ellipse2D.Double(border, border, width - border * 2, width - border * 2);
            //保留区域
            graphics.setClip(shape);
            graphics.drawImage(qqFace, border, border, width - border * 2, width - border * 2, null);
            graphics.dispose();
            //在圆图外面再画一个圆
            //新创建一个graphics，以保画的圆不会有锯齿
            graphics = qqFaceImage.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int border1 = 3;
            //画笔为4.5个像素
            //使画笔时基本会像外延伸一定像素，具体可以自己使用的时候测试
            Stroke s = new BasicStroke(5F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
            graphics.setStroke(s);
            graphics.setColor(Color.WHITE);
            graphics.drawOval(border1, border1, width - border1 * 2, width - border1 * 2);
            graphics.dispose();
            //画图
            Graphics newGraphics = img.getGraphics();
            newGraphics.drawImage(paImage.getScaledInstance(500,500,Image.SCALE_DEFAULT),0,0,null);
            newGraphics.drawImage(qqFaceImage.getScaledInstance(100,100,Image.SCALE_DEFAULT),0,400,null);

            //转换为输入流返回
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            ImageOutputStream imOut = ImageIO.createImageOutputStream(bs);
            ImageIO.write(img, "png", imOut);
            InputStream inputStream = new ByteArrayInputStream(bs.toByteArray());
            face.close();
            bs.close();
            imOut.close();
            return inputStream;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 缩小Image
     */
    public static BufferedImage scaleByPercentage(BufferedImage inputImage, int newWidth, int newHeight){
        // 获取原始图像透明度类型
        try {
            int type = inputImage.getColorModel().getTransparency();
            int width = inputImage.getWidth();
            int height = inputImage.getHeight();
            // 开启抗锯齿
            RenderingHints renderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            // 使用高质量压缩
            renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            BufferedImage img = new BufferedImage(newWidth, newHeight, type);
            Graphics2D graphics2d = img.createGraphics();
            graphics2d.setRenderingHints(renderingHints);
            graphics2d.drawImage(inputImage, 0, 0, newWidth, newHeight, 0, 0, width, height, null);
            graphics2d.dispose();
            return img;

        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 菜单
     * @return 返回菜单
     */
    public String menu(){
        return "舔狗日记 点歌\n" +
                "土味情话 涩图\n" +
                "福利姬     ";
    }

}
